"""Simple Flask web apa to visualize twitter sentiment and cloud words.

The webapp uses highcarts for front end visualization.
"""

from flask import Blueprint, render_template, jsonify, current_app, request
import psycopg2
import pandas as pd
import numpy as np
import time


home_page = Blueprint('home_page', __name__)


def keywords():
    """Get all keywords used to filter twitter from database.

    Returns
    -------
    list: Keywords
    """
    conn = psycopg2.connect(database=current_app.config['DB'],
                            user=current_app.config['USER'],
                            password=current_app.config['PASSWD'],
                            host=current_app.config['HOST'],
                            port=current_app.config['PORT']
                            )

    cur = conn.cursor()
    cur.execute("""SELECT * from keywords""")
    rows = cur.fetchall()
    rows = [val[0].replace('@', '').replace('#', '') for val in rows]
    rows = list(set(rows))
    rows = sorted(rows, key=lambda v: v.upper())

    conn.close()
    return rows


def tweets(keywords=None, timestamp=None, aggregation=None):
    """Get tweets data.

    Parameters
    ----------
    keywords : None, optional
        Filter by keyywords.
    timestamp : None, optional
        Filter by timestamp.
    aggregation : None, optional
        Aggregate.

    Returns
    -------
    tuple
        value, mean, wordcloud, aggregated wordcloud, tweet count
    """
    conn = psycopg2.connect(database=current_app.config['DB'],
                            user=current_app.config['USER'],
                            password=current_app.config['PASSWD'],
                            host=current_app.config['HOST'],
                            port=current_app.config['PORT']
                            )

    df = pd.read_sql_query("SELECT timestamp_ms, interesting_words, compound"
                           " FROM tweets WHERE timestamp_ms >= {}"
                           .format(timestamp), con=conn)

    if keywords and 'all' not in keywords:
        regex_str = '|'.join(keywords).replace(' ', '')
        df = df[df['interesting_words'].str.contains(regex_str, case=False)]

    if df.empty:
        return None, None, None, None, None

    df['timestamp_ms'] = df['timestamp_ms'].astype('int64')
    df['dfixs'] = pd.to_datetime(df['timestamp_ms'], unit='ms')
    df.index = pd.DatetimeIndex(df['dfixs'])
    conn.close()

    tweet_count = len(df)

    text = ' '.join(list(df['interesting_words']))
    words = text.split(',')
    words = list(filter(None, words))

    WORDCLOUD_WORDS_LIMIT = current_app.config['WORDCLOUD_WORDS_LIMIT']
    WORDCLOUD_CUP_WEIGHT = current_app.config['WORDCLOUD_CUP_WEIGHT']

    words_dict = {}
    for word in words:
        words_dict[word] = words_dict.get(word, 0) + 1

    try:
        sorted_by_value = sorted(words_dict.items(), key=lambda kv: kv[1])
        if len(sorted_by_value) > WORDCLOUD_WORDS_LIMIT + 1:
            sorted_by_value = sorted_by_value[-WORDCLOUD_WORDS_LIMIT:]
        df_wordcount = pd.DataFrame(
            sorted_by_value, columns=['name', 'weight'])
        if len(df) > WORDCLOUD_CUP_WEIGHT + 1:
            df_wordcount['weight'].iloc[:WORDCLOUD_CUP_WEIGHT] \
                = df_wordcount.iloc[WORDCLOUD_CUP_WEIGHT, 1]
        wordcloud = df_wordcount.to_dict(orient='records')
    except Exception as _:
        # empty wordlcoud
        wordcloud = {}

    df_compund = df.groupby(pd.TimeGrouper(aggregation))[
        'compound'].agg(['mean', 'std'])
    df_keywords = \
        df.groupby(pd.TimeGrouper(aggregation))[
            'interesting_words'].agg(lambda x: ','.join(x))
    df_compund['min'] = df_compund['mean'] - df_compund['std']
    df_compund['max'] = df_compund['mean'] + df_compund['std']
    df_compund['min'] = df_compund['min'].round(2)
    df_compund['max'] = df_compund['max'].round(2)
    df_compund['mean'] = df_compund['mean'].round(2)
    df_compund.dropna(subset=['mean', 'min', 'max'], inplace=True)

    df_compund['timestamp'] = df_compund.index.astype(np.int64) // 10 ** 6
    val = list(zip(df_compund['timestamp'],
                   df_compund['min'], df_compund['max']))
    mean = list(zip(df_compund['timestamp'], df_compund['mean']))

    text = df_keywords[-1]
    words = text.split(',')
    words = list(filter(None, words))

    words_dict = {}
    for word in words:
        words_dict[word] = words_dict.get(word, 0) + 1

    try:
        sorted_by_value = sorted(words_dict.items(), key=lambda kv: kv[1])
        if len(sorted_by_value) > WORDCLOUD_WORDS_LIMIT + 1:
            sorted_by_value = sorted_by_value[-WORDCLOUD_WORDS_LIMIT:]
        df_wordcount = pd.DataFrame(sorted_by_value,
                                    columns=['name', 'weight'])
        if len(df) > WORDCLOUD_CUP_WEIGHT + 1:
            df_wordcount['weight'].iloc[:WORDCLOUD_CUP_WEIGHT] = \
                df_wordcount.iloc[WORDCLOUD_CUP_WEIGHT, 1]
        wordcloud_agg_last = df_wordcount.to_dict(orient='records')
    except Exception as _:
        wordcloud_agg_last = {}

    return val, mean, wordcloud, wordcloud_agg_last, tweet_count


@home_page.route('/')
def index():
    """Index page.

    Returns
    -------
    html
        Index
    """
    return render_template('index.html', keywords=keywords(),
        history=current_app.config['PLOT_HISTORY'])


@home_page.route('/data', methods=["GET", "POST"])
def data():
    """Get twitter data.

    Returns
    -------
    json
        timestamp, min, max
    """
    args = request.args
    aggregation = args.get('aggregation')
    keywords = args.get('keywords').split(',')

    HISTORY = current_app.config['PLOT_HISTORY'] * 60 * 60 * 1000

    val, mean, wordcloud, wordcloud_agg_last, tweet_count = \
        tweets(keywords=keywords,
               timestamp=int(round(time.time() * 1000)) - HISTORY,
               aggregation=aggregation)

    return jsonify(val=val, mean=mean, wordcloud=wordcloud,
                   wordcloud_agg_last=wordcloud_agg_last,
                   tweetCount=tweet_count)


@home_page.route('/update', methods=["GET", "POST"])
def update():
    """Get new data on update.

    This function is called when update is triggered on webpage either
    by user or automatically.

    Returns
    -------
    json
        data
    """
    args = request.args
    aggregation = args.get('aggregation')
    keywords = args.get('keywords')

    HISTORY = current_app.config['PLOT_HISTORY'] * 60 * 60 * 1000

    val, mean, wordcloud, wordcloud_agg_last, tweet_count = \
        tweets(keywords=keywords,
               timestamp=int(round(time.time() * 1000)) - HISTORY,
               aggregation=aggregation)

    return jsonify(val=val, mean=mean, wordcloud=wordcloud,
                   tweetCount=tweet_count)
