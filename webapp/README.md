This app is part of docker compose for twitter sentiment.
It will visualize the harvested data in simple graph using
Flask python web microframework as backend and higcharts
as frontend.

No changes are needed.

Modification can be done in docker-compose.yaml for env variables.

