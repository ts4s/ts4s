"""
creating Flask app
"""

from flask import Flask

from .views.home import home_page


app = Flask(__name__)

app.config.from_pyfile('config.py', silent=True)

app.register_blueprint(home_page)
