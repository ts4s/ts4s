import os

USER = os.environ['POSTGRES_USER']
PASSWD = os.environ['POSTGRES_PASSWORD']
DB = os.environ['POSTGRES_DB']
HOST = os.getenv('POSTGRES_HOST', 'db')
PORT = os.getenv('POSTGRES_PORT', 5432)

PLOT_HISTORY = int(os.getenv('PLOT_HISTORY', 5))
WORDCLOUD_WORDS_LIMIT = int(os.getenv('WORDCLOUD_WORDS_LIMIT', 30))
WORDCLOUD_CUP_WEIGHT = int(os.getenv('WORDCLOUD_CUP_WEIGHT', 20))
