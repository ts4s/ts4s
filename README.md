# Twitter Sentiment for Science

Tool to find sentiment of people talking on twitter for selected words.

## Getting Started

These instructions will get you a copy of the project up and running on
your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

If you are using RaspberryPi go to [Deploying on RaspberryPi](#raspi) section.

### Prerequisites
* **Docker**:
Instruction to install https://docs.docker.com/install/

* **Docker Compose**:
Instruction to install
https://docs.docker.com/compose/install/


### Usage

#### <a name="start_dockers"></a> Starting containers
1. In terminal clone the repositor and navigate to the project root directory.

2. Set environmental variables

    * Rename twitter.template to twitter.env and set the variable from
        your dev twitter account.

    * Rename database.template to database.env and change the variables
       or leave them with defaults.

3. Set keywords to watch and analyze

   Modify words.yaml file to set keywords and additional stopwords.

4. <a name="start_dockers_s4"></a> Start docker compose

    a. Start postgres docker in detach mode

    <code>docker-compose up -d</code>

    In detach mode the dockers run as deamons and no output is visible in terminal.
    If you want to see the log output run:

    <code>docker-compose logs</code>


    b. start docker in no-detach mode

    <code>docker-compose up</code>

    Alternatively start docker in no-detach mode to see the logs output directly in
    your terminal. This is good for debugging for example.

The docker compose will start and run 4 containers:
* [postgres](https://hub.docker.com/_/postgres) - 3rd party docker image
* [adminer](https://hub.docker.com/_/adminer/) - 3rd party docker image
* [harvester](https://gitlab.com/ts4s/ts4s/container_registry) - custom docker image
* [webapp](https://gitlab.com/ts4s/ts4s/container_registry) - custom docker image


#### Interactive visualization
To visualize twitter sentiment open browser and navigate to
http://localhost:5555/

#### Connect to postgres

1. using [adminer](https://www.adminer.org/)

    Open browser at url http://localhost:8080.

    The login page should look like this (password is specified in database.env file):


    ![alt text](login.png
     "Login page")

    If you have changed credentials in database.env file you must change login page accordingly.


2. using command line

    Open terminal (PostgreSQL client must be installed on you machine)

    <code>psql -h localhost -p 5431 -U postgres -W</code>

    Credentials are same as specified in database.env file.


#### Changing / updating words to track and analyze

The ```words.yaml``` file contains keywords to track and stopwords to add
[nltk](https://www.nltk.org/) default stopwords.


Twitters which contains the keywords will be stored in PostgreSQL database.

There are two posibilities to manipulate keywords/stopwords: updating words to
track more words (add more stopwords respectively) and tracking different set
of words. The first option will not remove stored twitters while the second
option requires to remove all stored twitter and start to store new twitter
with different keywords.

* **updating keywords**
    In order to update keywords and continue the harvesting the words.yaml
    file must be modifed and dockers must be restarted.
    Steps:
    In terminal navigate to project directory and run
    1. <code>docker-compose down</code>
    2. Modify ```words.yaml``` file
    3. Start dockers (see section [Starting dockers section 4](#start_dockers_s4))

* **track different words**
    In order to start track different set of keywords the old twitter
    stored in PostgreSQL must be removed.
    Steps:
    In terminal navigate to project directory and run
    1. <code>docker-compose down -v</code>

        _-v will remove mounted volumes_
    2. Modify ```words.yaml``` file
    3. Start dockers (see section [Starting dockers section 4](#start_dockers_s4))


## Built With

* [Docker CE](https://docs.docker.com/install/) - The container platform
* [Postgres Docker Image](https://hub.docker.com/_/postgres) - Object-relational database system
* [Adminer Docker Image](https://hub.docker.com/_/adminer/) - Web tool for managing content in PostgreSQL database
* [Python](https://www.python.org/) - Main programming language
* [NLTK](https://www.nltk.org/) -  Platform for building Python programs to work with human language data.
* [Flask](http://flask.pocoo.org/) - Micro web framework written in Python
* [Higcharts](https://www.highcharts.com/) - JavaScript charting library based on SVG

## Development

The project contains both custom images with cource code which can be modified:
* **harvester** - python app to harvest twitter based on keywords, store twitters
and calculate sentiment.

* **webapp** - simple Flask web app to visualize results from harvester in browser

When modifying source code the docker must be started with --build option:

<code>docker-compose up --build</code>


## Authors

* **Konstantin Stadler** - *Initial work* - [NTNU](https://www.ntnu.edu/employees/konstantin.stadler)
* **Radek Lonka** - *Initial work* - [NTNU](https://www.ntnu.edu/employees/radek.lonka)


## License

This project is licensed under the [The 3-Clause BSD License](https://opensource.org/licenses/BSD-3-Clause)


## <a name="raspi"></a>![alt text](RPi_Logo.png) Deploying on RaspberryPi


### Installing OS on the RapsberrrPi

Installation guide: https://www.raspberrypi.org/documentation/installation/installing-images/

Using [NOOBS](https://www.raspberrypi.org/documentation/installation/noobs.md) system.

This are steps as described in the NOOBS README file from official site:
<em>
NOOBS INSTALLATION INSTRUCTIONS

1. Insert an SD card that is 8GB or greater in size into your computer.
2. Format the SD card using the platform-specific instructions below:
   a. Windows (32GB cards and under)
      i. Download the SD Association's Formatting Tool from https://www.sdcard.org/downloads/formatter_4/eula_windows/
      ii. Install and run the Formatting Tool on your machine
      iii. Check that the SD card you inserted matches the one selected by the Tool
      iv. Click the "Format" button
   b. Mac (32GB cards and under)
      i. Download the SD Association's Formatting Tool from https://www.sdcard.org/downloads/formatter_4/eula_mac/
      ii. Install and run the Formatting Tool on your machine
      iii. Select "Overwrite Format"
      iv. Check that the SD card you inserted matches the one selected by the Tool
      v. Click the "Format" button
   c. Linux
      i. We recommend using gparted (or the command line version parted)
      ii. Format the entire disk as FAT32
   d. Cards over 32GB
      i. Follow the instructions on https://www.raspberrypi.org/documentation/installation/sdxc_formatting.md
3. Extract the files contained in this NOOBS zip file.
4. Copy the extracted files onto the SD card that you just formatted so that this file is at the
   root directory of the SD card. Please note that in some cases it may extract the files into a folder,
   if this is the case then please copy across the files from inside the folder rather than the folder itself.
5. Insert the SD card into your Pi and connect the power supply.

Your Pi will now boot into NOOBS and should display a list of operating systems that you can choose to install.
If your display remains blank, you should select the correct output mode for your display by pressing
one of the following number keys on your keyboard:
1. HDMI mode - this is the default display mode.
2. HDMI safe mode - select this mode if you are using the HDMI connector and cannot see anything
   on screen when the Pi has booted.
3. Composite PAL mode - select either this mode or composite NTSC mode if you are using the
   composite RCA video connector.
4. Composite NTSC mode

If you are still having difficulties after following these instructions, then please visit
the Raspberry Pi Forums ( http://www.raspberrypi.org/forums/ ) for support.
</em>

**NOTE:**
After booting to RaspberryPi first time we choose
**Raspbian. A port of Debian with the Raspeberry Pi Desktop**
for the next steps to work. However any Debian based ditributions should work too.

### Installing docker and docker-compose
As described here: https://www.raspberrypi.org/blog/docker-comes-to-raspberry-pi/
The docker can be installed by runing this command in terminal:

<code>curl -sSL https://get.docker.com | sh</code>

To use Docker as non-root user run this command:

<code>sudo usermod -aG docker pi</code>

and restart the RapsberryPi.

To minstall docker compose we shouyld updated teh repository:

<code>sudo apt update</code>

install python3 and pip3

<code>sudo apt install python3 pip3</code>

and finally install docker-compose

<code>sudo pip3 install docker-compose</code>

Since we are running on arm architecture we have to specify the python base image
which will be used during harvester and webapp build process by runnig this command:

<code>EXPORT BASE_IMAGE=arm32v7/python:3.7</code>

The rest is same as in previouse [Starting containers](#start_dockers) section
