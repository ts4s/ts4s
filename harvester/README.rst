*****************
Twitter Harvester
*****************

Docker python app for havesting tweets based on keywords. 
Requres PostgreSQL to store harvested data.

Configuration and setup
=======================

Build docker
============
Navigate to harvester where Dockerfile is and run:
``docker build -t harvester:latest .`` 

Run the image:
``  ``


------OLD-----------
Configuration and setup
=======================

#) Setup a DB server for storing tweets.  
   
   The twitter_harvester.py stores data in a SQL database. 
   It was developed for working with PostgreSQL, 
   other SQL DB might work as well (untested). 

#) Clone to the repository

#) Update your requirements:

   In the cloned directory run 
   ``pip install -r requirements.txt``.

   Use a virtual environment in case of conflicts or to 
   keep your python environment clean.

#) Download the required nltk dictionaries:

   Open a python console and run

   ``import nltk``

   ``nltk.download('stopwords')``

   ``nltk.download('vader_lexicon')``

   ``exit()``

#) Update config_logging.yaml 

   Defines the logging levels and where the logfiles should be stored

#) Update config_startup.yaml 

   Defines the keywords to be tracked

#) Create secrets.yaml 

   This file does not exist in the repository.
   Use the secrets_template.yaml as an example:

   - Add your `twitter access token, key and secret.`_
   - Add the credentials to your database. 

.. _twitter access token, key and secret.: https://developer.twitter.com/en/docs/basics/authentication/guides/access-tokens.html
         
#) Create the required tables in your db: 

   The file 'sql_helper.py' consists of helper functions for managing the DB. 
   For the initial start, run the function 'create_table' to setup the db.

#) Setup crontab for the user running the script
   
   If the user running the twitter_harvester never used crontab, 
   initialize crontab by running ``crontab -e`` for this user.

Starting the program
=====================

The twitter_harvester is managed with a shell script:
Run 

.. code:: bash

   ./harvester_manager.sh start

to start the program.


More information is available in the help of the shell script:

.. code:: bash

   ./harvester_manager.sh help

Checking the program during runtime
===================================

To get the current status of the program use

.. code:: bash

   ./harvester_manager.sh status
  
In the file 'sql_helper.py' you will also find a function which
returns the current number of gathered tweets (
   
.. code:: python

   ./harvester_manager.sh status
   gathered_tweets = count_entries(conn, table='tweets')

Changing settings during runtime 
================================

In the file 'config_realtime.yaml' additional stopwords 
can be included to further filter the results obtained from twitter.
This file is read permanently and can be updated during runtime.

