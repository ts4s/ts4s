"""
Main script.

The script downloads stopwords from nltk module, reads config file,
create database tables if they do not exists and run harvester
to harvest tweets based on config keywords and stopwords.
"""

import os
from sql_helper import create_tables
import harvester
import nltk

if __name__ == "__main__":
    """
    Main script.

    Input file:
        config_words.yaml

    The environment variables:
        POSTGRES_DB
        POSTGRES_USER
        POSTGRES_PASSWORD
        POSTGRES_HOST defaults to db
        POSTGRES_PORT defaults to 5432

        TWITTER_CONSUMER_KEY
        TWITTER_CONSUMER_SECRET
        TWITTER_ACCESS_TOKEN
        TWITTER_ACCESS_SECRET

        NUMBER_OF_THREADS defaults to 5
        PROCESSING_QUEUE_SIZE defaults to 100
    """

    nltk.download('stopwords')
    nltk.download('vader_lexicon')

    db_secrets = {}
    db_secrets['name'] = os.environ['POSTGRES_DB']
    db_secrets['user'] = os.environ['POSTGRES_USER']
    db_secrets['password'] = os.environ['POSTGRES_PASSWORD']
    db_secrets['host'] = os.getenv('POSTGRES_HOST', 'db')
    db_secrets['port'] = os.getenv('POSTGRES_PORT', 5432)

    twitter_secrets = {}
    twitter_secrets['consumer_key'] = os.environ['TWITTER_CONSUMER_KEY']
    twitter_secrets['consumer_secret'] = os.environ['TWITTER_CONSUMER_SECRET']
    twitter_secrets['access_token'] = os.environ['TWITTER_ACCESS_TOKEN']
    twitter_secrets['access_secret'] = os.environ['TWITTER_ACCESS_SECRET']

    secrets = {
        'twitter': twitter_secrets,
        'database': db_secrets
    }

    conn = harvester.setup_db_connection(secrets['database'])
    create_tables(conn)
    conn.close()

    config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'words.yaml')
    number_of_threads = int(os.getenv('NUMBER_OF_THREADS', 5))
    processing_queue_size = int(os.getenv('PROCESSING_QUEUE_SIZE', 100))
    config = harvester.read_config(cfg_file=config_file)

    harvester.main(secrets, config, number_of_threads, processing_queue_size)
