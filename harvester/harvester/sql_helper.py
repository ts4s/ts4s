""" Setup SQL tables

This script creates the tables needed for storing the tweets.

NOTE: Commands in here can be dangerous
"""

import logging

# from harvester import SECRET_FILE
from harvester import read_config
from harvester import setup_db_connection

from config_sql_tables import TABLES
from config_sql_tables import INDEXES


def count_entries(conn, table):
    """ Count entries (rows) in table
    """
    cur = conn.cursor()
    size_command = f"SELECT COUNT(*) FROM {table}"
    cur.execute(size_command)
    return cur.fetchone()[0]


def drop_table(conn, table):
    """ Delete a table to reset the db

    Parameters
    -----------
        conn: db connection
        table: Str or list
            of tables to delete
    """
    cur = conn.cursor()

    if type(table) is str:
        table = [table]

    for tt in table:
        cur.execute(f"DROP table {tt};")

    conn.commit()
    return locals()


def create_tables(conn):
    """ Create the tables in the db as needed for the harvester
    """
    cur = conn.cursor()

    for table_name, table_columns in TABLES.items():
        command = f"CREATE TABLE IF NOT EXISTS {table_name} ("
        sep = ", "
        for col_name, col_para in table_columns.items():
            command = command + col_name + " " + " ".join(
                col_para.values()) + sep
        command = command.rstrip(sep) + ");"

        try:
            logging.info(f"Execute command: {command}")
            cur.execute(command)
        except Exception as e:
            logging.error("Can not execute command: {}".format(e))

    for ixs in INDEXES:
        try:
            command = f"CREATE INDEX IF NOT EXISTS {ixs['column']}_ixs  ON {ixs['table']} ({ixs['column']});"
            logging.info(f"Execute command: {command}")
            cur.execute(command)
        except Exception as e:
            logging.error("Can not execute command: {}".format(e))

    try:
        ret_value = conn.commit()
        if ret_value:
            logging.info(f"Commited with return {ret_value}")
    except Exception as e:
        logging.error("Can not commit: {}".format(e))

    return locals()


def main():
    """ Just for calling the choosen function
    """
    #db_sec = read_config(cfg_file=SECRET_FILE)['database']
    #conn = setup_db_connection(db_sec)

    #gathered_tweets = count_entries(conn, table='tweets')
    #number_keywords = count_entries(conn, table='keywords')

    # RESET DB - TAKE CARE!
    # drop_table(conn, TABLES.keys())
    #create_tables(conn)

    #conn.close()
    #return locals()
    pass

if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    try:
        locals().update(main())
    except Exception as e:
        logging.exception(e)
        raise
