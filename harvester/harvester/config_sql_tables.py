""" Global setup for the sql tables and columns

Note
----

This is a python setup file in order to reuse the 'USER_SUB_DICT_ID'
in the key names.
"""

USER_SUB_DICT_ID = 'user_'

# Entries for each table, with dict for each column
TABLES = {
    'keywords' : {
        'word': {
            'type': 'varchar',
            'constraints': 'PRIMARY KEY',
        }
    },
    'tweets' : {
        'id' : {
            'type': 'bigint',
            'constraints': 'PRIMARY KEY',
        },
        'text' : {
            'type': 'varchar',
        },
        'created_at' : {
            'type': 'varchar',
        },
        'timestamp_ms' : {
            'type': 'bigint',
        },
        'lang' : {
            'type': 'varchar',
        },
        'in_reply_to_status_id_str' : {
            'type': 'varchar',
        },
        'geo': {
            'type': 'varchar'
        },
        'coordinates': {
            'type': 'varchar'
        },
        USER_SUB_DICT_ID+'id' : {
            'type': 'varchar',
        },
        USER_SUB_DICT_ID+'name' : {
            'type': 'varchar',
        },
        USER_SUB_DICT_ID+'screen_name' : {
            'type': 'varchar',
        },
        USER_SUB_DICT_ID+'location' : {
            'type': 'varchar',
        },
        USER_SUB_DICT_ID+'count' : {
            'type': 'varchar',
        },
        # list of words for tagcloud and keyword select
        'interesting_words' : {
            'type': 'varchar'
        },
        'sentiment_clean_text' : {
            'type': 'varchar'
        },
        'neg' : {
            'type': 'float'
        },
        'neu' : {
            'type': 'float'
        },
        'pos' : {
            'type': 'float'
        },
        'compound' : {
            'type': 'float'
        },
    }
}


INDEXES = [
        {
        'table': 'tweets',
        'column': 'timestamp_ms'
        }
        ]


