"""
Main script for harvesting tweets based on keywords.

IEDL 2018
"""

import logging
import logging.config
import json
import re
import os
import threading
import queue
import psycopg2

import yaml
import tweepy
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer

from config_sql_tables import TABLES
from config_sql_tables import USER_SUB_DICT_ID

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))
CONFIG_LOGGING = os.path.join(SCRIPT_PATH, 'config_logging.yaml')


class TwitterListener(tweepy.streaming.StreamListener):
    """
    TODO

    Write some dockstring.
    """

    def __init__(self, tweet_proc_queue):
        """
        TODO
        """
        self.counter_tweets = 0
        self.tweet_proc_queue = tweet_proc_queue

    def on_data(self, data):
        # We use on_data here and not on_status
        # b/c there is a bug in on_status in tweepy
        # which relies on some self.api which is
        # not present.
        # However, this means that we have to
        # parse the string to dict ourselves.
        try:
            if not self.counter_tweets % 100:
                logging.info(f"Processed {self.counter_tweets} tweets")
            try:
                self.tweet_proc_queue.put(data, block=False)
                logging.debug(f"Put tweet into queue - queue size: "
                              f"{self.tweet_proc_queue.qsize()}")
            except queue.Full:
                logging.error("Processing Queue full - "
                              "missed tweet: {}".format(data))

            self.counter_tweets += 1
            return True
        except BaseException as e:
            logging.error("{}".format(str(e)))
        return True

    def on_error(self, status):
        logging.error("Error from Twitter: {}".format(status))
        return True


def tweet_processing(proc_queue, db_secrets, stopwords):
    """ Worker for processing tweets

    """
    while True:
        try:
            raw_data = proc_queue.get()
            logging.debug(f"Got entry from queue - "
                          f"remaining queue size: {proc_queue.qsize()}")

            tweet_dict = json.loads(raw_data)
            text_clean = clean_text(tweet_dict['text'])
            tweet_dict.update(get_sentiment(text_clean))

            tweet_dict['interesting_words'] = ','.join(
                get_interesting_words(tweet_dict['text'], stopwords))

            # Add the user sub dict to the root dict
            user_dict = {USER_SUB_DICT_ID+key: value for key, value in
                         tweet_dict.get('user', {}).items()}
            tweet_dict.update(user_dict)

            write_tweet_to_db(db_sec=db_secrets,
                              tweet_dict=tweet_dict)
        except Exception as e:
            logging.error(f"Problem processing tweet: {e}")
        finally:
            try:
                proc_queue.task_done()
                logging.debug("Tweet processing finished")
            except ValueError:
                pass


def get_interesting_words(text, stopwords):
    """ Extracts the words interesting for a tag cloud or classification
    """
    # words = nltk.tokenize.word_tokenize(text)
    # Using split here to keep the hashtags and usernames together
    words = text.replace('.', ' ').replace('"', ' ').split()
    words_restricted = {w.lower() for w in words if
                        ((w.lower() not in stopwords) and
                         (2 < len(w)) and
                         ('//' not in w) and
                         (w != '&'))}

    logging.debug(f"Extracted interesting words: "
                  f"{', '.join(words_restricted)}")

    return words_restricted


def get_stopwords(cfg):
    """ Generates an updated list of stopwords based on the content of cfg

    The full list of stopwords contains of the nltk corpus stopwords
    plus the list found in the cfg additional_stopwords keyword.
    """
    stopwords = nltk.corpus.stopwords.words('english')
    custom_stopwords = cfg['additional_stopwords']
    return stopwords + custom_stopwords


def read_twitter_streams(auth, tweet_proc_queue, keywords):
    """ Read the twitter streams

    Parameters
    ----------
    auth: tweepy.OAuthHandler
        Initated with app tokens and secrets
    keywords: list
        Words to filter for
    """
    twitter_stream = tweepy.Stream(
        auth=auth,
        listener=TwitterListener(tweet_proc_queue=tweet_proc_queue))
    twitter_stream.filter(track=keywords)


def read_config(cfg_file):
    """ Read the config yaml files
    """
    with open(cfg_file, 'r') as ff:
        content = yaml.load(ff)
    return content


def ensure_dir_existens(log_dict):
    """ Ensures that the dir for the log file exists

    Parameters
    ----------
    log_dict: dict
        Dictionary for logging setup as required
        by logging.config.dictConfig
    """
    for kk, it in log_dict['handlers'].items():
        if 'filename' in it:
            try:
                log_path = os.path.dirname(os.path.realpath(it['filename']))
                os.makedirs(log_path, exist_ok=True)
            except PermissionError:
                raise PermissionError(f"Permission denied for creating "
                                      f"directory {log_path} for log files")


def setup_twitter_authentification(twitter_secrets):
    """ Builds auth object for the streamer

    Parameters:
    ----------
        twitter_secrets: dict
            with
                - consumer_key
                - consumer_secret
                - access_token
                - access_secret

    """
    auth = tweepy.OAuthHandler(twitter_secrets['consumer_key'],
                               twitter_secrets['consumer_secret'])
    auth.set_access_token(twitter_secrets['access_token'],
                          twitter_secrets['access_secret'])
    return auth


def setup_db_connection(db_para):
    """ Connects to db and returns connection
    """
    conn = psycopg2.connect(database=db_para['name'],
                            user=db_para['user'],
                            password=db_para['password'],
                            host=db_para['host'],
                            port=db_para['port'])
    return conn


def get_sentiment(text):
    """ Sentiment scores for the string parameter text

    This is based on the VADER sentiment analyser.

    Parameters
    ----------
    text : string
        The text to analyse.

    Returns
    -------
    dict with scores (keys: compound, neg, neu, pos)

    """
    score = SentimentIntensityAnalyzer().polarity_scores(text)
    return score


def clean_text(text):
    """ Cleans the string text for the sentiment analysis
    """
    replace_patterns = [
        ('@', ''),
        ('#', ''),
        ('(http|https)://[^\s]*', ''),
        ]

    text = text.lower()
    for tl_replace in replace_patterns:
        text = re.sub(*tl_replace, string=text)
    return text


def write_keywords_to_db(db_secrets, keywords):
    """ Add the current filter keywords to db table keywords
    """
    db_conn = setup_db_connection(db_secrets)
    cur = db_conn.cursor()
    for ww in keywords:
        cur.execute(f"INSERT INTO keywords (word) VALUES ('{ww}') "
                    "ON CONFLICT (word) DO NOTHING")
    db_conn.commit()
    cur.close()
    db_conn.close()
    logging.debug("Wrote keywords to database")


def write_tweet_to_db(db_sec, tweet_dict):
    """ Add the current tweet data to the db
    """
    db_conn = setup_db_connection(db_sec)

    col_names = [key for key in TABLES['tweets'].keys()
                 if key in tweet_dict.keys()]
    col_values = ["'{}'".format(
        str(tweet_dict[key]).replace("'", "''")) for key in col_names]

    str_col_names = ', '.join(col_names)
    str_col_values = ', '.join(col_values)

    cur = db_conn.cursor()
    try:
        cur.execute(
            f"INSERT INTO tweets ({str_col_names}) VALUES ({str_col_values})")
    except Exception as e:
        logging.error(f"Did not write because of {e}")

    db_conn.commit()
    cur.close()
    db_conn.close()
    logging.debug("Wrote tweet to database")


def main(secrets, cfg, number_of_threads=5, processing_queue_size=100):
    """Main function

    Harvest twitters based on config input and save them to PostgreSQL.

    Parameters
    ----------
    secrets : dict
        Database and twitter credentials.
    cfg : dict
        Keywords amd additionla stopwrods to filter form twitters.
    number_of_threads : int, optional
        Number ot threads for processing twitters requests.
        Defaults to 5.
    processing_queue_size : int, optional
        Processing queue for twitter requests. Defults to 100.

    Raises
    ------
    Exception
        Any exception is logged and reraised.
    """
    log_cfg = read_config(cfg_file=CONFIG_LOGGING)
    ensure_dir_existens(log_cfg)
    logging.config.dictConfig(log_cfg)
    logging.debug(f"Setup logging as defined in {CONFIG_LOGGING}")
    logging.info(f"Run as PID: {os.getpid()}")

    write_keywords_to_db(db_secrets=secrets['database'],
                         keywords=cfg['tracking_keywords'])

    twitter_auth = setup_twitter_authentification(secrets['twitter'])

    stopwords = get_stopwords(cfg)
    processing_queue = queue.Queue(maxsize=processing_queue_size)
    logging.info("Setup for recording completet")

    try:
        logging.info("Starting thereads")
        for thread_nr in range(number_of_threads):
            thread = threading.Thread(
                name=f'Processing_Tweet_Thread_{thread_nr}',
                target=tweet_processing,
                args=(processing_queue,
                      secrets['database'],
                      stopwords))
            thread.daemon = True
            thread.start()

        logging.info("Read write twitter")
        read_twitter_streams(auth=twitter_auth,
                             tweet_proc_queue=processing_queue,
                             keywords=cfg['tracking_keywords'])

    except Exception as e:
        logging.warning(f"Exception {e}")
        raise Exception(e)

    finally:
        logging.debug(
            "Processing remaining {} entries in the processing queue".format(
                processing_queue.qsize()))
        processing_queue.join()
        logging.info("Processed all recorded tweets")

    return locals()


if __name__ == "__main__":
    try:
        locals().update(main())
    except Exception as e:
        logging.critical(e)
